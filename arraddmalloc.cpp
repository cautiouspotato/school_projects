#include <iostream>
#include <cstring>

//not elegant but hey
int A[5] = {1, 2, 3, 4, 5};
int B[4] = {9, 8, 7, 6};
int N = 9; // the length you want array C to be it should be A+B
int tmp;

int main() {
	//add the two arrays first
	int* C = malloc(N * sizeof(int)); // array to hold the result

	memcpy(C,     A, 5 * sizeof(int)); // copy 5 ints from A
	memcpy(C + 4, B, 4 * sizeof(int)); // copy 4 ints from B


	//bubble sort em descending
	for( int i = 0; i < N - 1; i++ ) {
		for( int j = 0; j < N - 1; j++ ) {
			//if current is less than next: swap
			if(C[j] < C[j + 1]) {
				tmp = C[j];
				C[j] = C[j + 1];
				C[j + 1] = tmp;
			}

		}
	}

	std::cout << "sorted descending" << std::endl;
	for( int i = 0; i < N; i++ ) {
		std::cout << C[i] << std::endl;
	}
}

#include<iostream>

int main() {
	int size, num, sum = 0;
	int avg = 0;
	
	std::cout << "how many numbers do you want to add: ";
	std::cin >> size;
	std::cout << "enter the numbers: \n";
	for(int i = 0; i < size; i++) {
		std::cin >> num;
		sum = sum + num;
	}
	avg = sum/size;
	
	std::cout << "Summed numbers: " << sum << std::endl;
	std::cout << "Average of numbers: " << avg << std::endl;
	
	return 0;
}


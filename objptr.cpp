// object pointers
#include <iostream>

class Student {
    private:
        int regno;
        char name[20];
        float fees;

    public:
        void get();
        void display();
};

void Student::get() {
    std::cout << "name: ";
    std::cin >> name;
    std::cout << "reg num: ";
    std::cin >> regno;
    std::cout << "fees: ";
    std::cin >> fees;
}

void Student::display() {
    std::cout << "name " << name << '\n';
    std::cout << "reg num " << regno << '\n';
    std::cout << "fees " << fees << '\n';
}

int main(int argc, char const *argv[]) {
    Student s, *sp;
    sp = &s;
    sp -> get();
    sp -> display();

    return 0;
}

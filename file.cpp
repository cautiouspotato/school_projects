// why the hecc isn't the default file format .txt who the hell uses .dat files?
#include<iostream>
#include<fstream>

// kinda a skeleton describing the stuff that
// goes in and out of the file
class store {
    int storeno;
    char storename[50];
    float area;
public:
    void inData() {
        std::cout << "number: ";
        std::cin >> storeno;
        std::cout << "name: ";
        std::cin >> storename;
        std::cout << "area: ";
        std::cin >> area;
    }

    void outData() {
        std::cout << storeno << "|" << storename << "|" << area <<'\n';
    }
};

int main(int argc, char const *argv[]) {
    std::fstream file;
    store store;
    int n;
    file.open("store.dat", std::ios::in | std::ios::out | std::ios::binary);

    std::cout << "how many records: ";
    std::cin >> n;

    // fill up the file
    for (int i = 1; i <= n; i++) {
        store.inData();
        file.write((char*) & store, sizeof(store));
    }

    //display the stuff from the file
    file.seekg(0, std::ios::beg); // go to the start of the file
    while (file.read((char*) & store, sizeof(store))) {
        store.outData();
    }

    file.close();
    return 0;
}

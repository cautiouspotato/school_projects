// takes the two arrays and sticks em together
// then bubble sorts them
// could make it accept array from human, but im lazy
#include <iostream>

int main() {
	// not elegant but hey
	int A[] = {1, 2, 3, 4, 5};
	int B[] = {10, 9, 8, 7, 6};
	int C[10];

	int N = 10; // the length of array C
	int tmp;

	// add A to the start of C
	for (int i = 0; i < 5; i++) {
		C[i] = A[i];
	}

	// start at the end of the last element of A
	for (int j = 5; j < 10; j++) {
		C[j] = B[j - 5]; // B[index - length of B], we only want the 5 offset for C
	}


	// bubble sort em descending
	for (int i = 0; i < N - 1; i++) {
		for (int j = 0; j < N - 1; j++) {
			// if current is less than next: swap
			if (C[j] < C[j + 1]) {
				tmp = C[j];
				C[j] = C[j + 1];
				C[j + 1] = tmp;
			}
		}
	}

	std::cout << "sorted descending" << std::endl;
	for( int i = 0; i < N; i++ ) {
		std::cout << C[i] << std::endl;
	}
}

#include<iostream>

int main() {
	float num[100], sum = 0.0, average;
	int n;
	
	std::cout << "Enter size of data: ";
	std::cin >> n;
	//make sure the array size entered is 1 to 100 long
	while(n > 100 || n <= 0) {
		std::cout << "Error! number should be in range 1-100";
		std::cout << "Reenter number: ";
		std::cin >> n;
	}
	//get the stuff to put in the array
	for(int i = 0; i < n; i++) {
		std::cout << i+1 << " Enter number: ";
		std::cin >> *(num+i);
		sum += *(num+i);
	}
	//display the array
	for(int i = 0; i < n; i++) {
		std::cout << " " << *(num+i);
	}	
	average = sum / n;
	std::cout << "\nAverage = " << average << std::endl;
	return 0;
}

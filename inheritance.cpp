// inhereting classes, a bad example
#include <iostream>
#include <string>

class Prisoner {
private:
    int roll;
    std::string name;
public:
    void read() {
        std::cout << "name: ";
        std::getline(std::cin, name);
        std::cout << "roll: ";
        std::cin >> roll;
    }

    void display() {
        std::cout << "name: " << name << '\n';
        std::cout << "roll: " << roll << '\n';
    }
};

class Prison : public Prisoner {
private:
    int v1, v2, v3, total;
public:
    void read1() {
        std::cout << "memes 1, 2, and 3: " << '\n';
        std::cin >> v1 >> v2 >> v3;
        total = v1 + v2 + v3;
    }

    void display1() {
        std::cout << "yeet: " << v1 << '\n';
        std::cout << "what: " << v2 << '\n';
        std::cout << "why: " << v3 << '\n';
        std::cout << "total: " << total << '\n';
    }
};

int main(int argc, char const *argv[]) {
    Prison ob;
    ob.read();
    ob.read1();
    ob.display();
    ob.display1();

    return 0;
}

// who comes up with these?
#include<iostream>

int main() {
	int year;

	std::cout << "Enter a year: ";
	std::cin >> year;

	if(year % 4 == 0) {
		if(year % 100 == 0) {
			if(year % 400 == 0) {
				std::cout << year << " is a leap year" << std::endl;
			} else {
				std::cout << year << " isn't a leap year" << std::endl;
			}
		} else {
			std::cout << year << " is a leap year" << std::endl;
		}
	} else {
		std::cout << year << " isn't a leap year" << std::endl;
	}

	return 0;
}

#include<iostream>

class History {
private:
	int recordno;
	std::string source;
	std::string location;
	int year;

public:
	History ();
	void disp();
};

// constructor
History::History(void) {
	recordno = 1001;
	source = "indus valley";
	location = "NULL";
	year = 234;
}

// display
void History::disp() {
	std::cout << recordno << '\n';
	std::cout << source << '\n';
	std::cout << location << '\n';
	std::cout << year << '\n';
}

int main() {
	History h;
	h.display();

	return 0;
}

// inline functions
#include <iostream>
#include <cmath>

inline int cube(int a) {
    return(a*a*a);
}

int main(int argc, char const *argv[]) {
    int x, y;

    std::cout << "enter a number: ";
    std::cin >> x;

    y = cube(x);
    std::cout << x << " cube is " << y << '\n';

    return 0;
}

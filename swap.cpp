// swap 
#include<iostream>

int main() {
	int one, two, swap;
	
	std::cout << "Enter two numbers: ";
	std::cin >> one >> two;
	
	swap = one;
	one = two;
	two = swap;
	
	std::cout << "swapped: " << one  << " " << two << std::endl;
	
	return 0;
}


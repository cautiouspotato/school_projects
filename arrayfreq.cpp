// the usual array finder program, but written with classes

#include <iostream>

class Frequency {
private:
    int arraySize, m[100], element, freq;
public:
    void getData();
    void display();
    void findData();
};

// fill up the array
void Frequency::getData() {
    std::cout << "Enter the size of the array: ";
    std::cin >> arraySize;
    std::cout << "Enter " << arraySize << " elements in the array" << '\n';

    for (int i = 0; i < arraySize; i++) {
        std::cin >> m[i];
    }
    // also get the item to be searched here
    std::cout << "what to search for: ";
    std::cin >> element;
}

// finds the thing you were looking for
void Frequency::findData() {
    freq = 0;

    for (int i = 0; i < arraySize; i++) {
        if(m[i] == element) {
            freq ++;
        }
    }
}

// display thingy
void Frequency::display() {
    std::cout << "Frequency of " << element << ": " << freq << '\n';
}

int main() {
    Frequency F;
    F.getData();
    F.findData();
    F.display();

    return 0;
}

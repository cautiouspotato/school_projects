#include<iostream>

int main() {
	int num = 0, num1 = 0, num2 = 0, digit = 0, cube;
	std::cout << "Enter a three digit number: ";
	std::cin >> num;
	num1 = num;
	
	//pretty clever, you delete the tenths place after every loop
	//resulting in the number eventually being 0
	while(num1 != 0) {
		//take the number in tenths place xx*x*
		digit = num1 % 10;
		//cube it
		cube = (digit * digit * digit);
		//add the cube to num2
		num2 = num2 + cube;
		//remove the number at tenths place xx*x*
		num1 = num1/10;
	}
	
	if(num == num2) {
		std::cout << num << " is an armstrong number\n";
	} else {
		std::cout << num << " isn't an armstrong number\n";
	}
	
	return 0;
}


//calculates factorials
#include<iostream>

int main() {
	float n, factorial = 1; // start at one or the whole thing returns 0
	std::cout << "Enter a number: ";
	std::cin >> n;
	
	for(int i = 1; i <= n; i ++) {
		factorial = factorial * i;
	}
	std::cout << n << " factorial is: " << factorial << std::endl;
	
	return 0;
}


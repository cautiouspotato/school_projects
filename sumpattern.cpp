// sum of the 1+3+5+7+9+n pattern
#include<iostream>

int main() {
	int n, result;
	std::cout << "gimme a number: ";
	std::cin >> n;

	for (int i = 1; i < n; i++) {
		if (i % 2 == 1) {
			result += i;
		}
	}
	std::cout << result << '\n';
}

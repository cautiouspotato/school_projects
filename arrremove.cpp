// deleting stuff from arrays
#include<iostream>

// just so we don't use global vars
class Terminate {
private:
    int n, m[100], element, position;
public:
    void getData();
    void display();
    void remove();
};

void Terminate::getData() {
    std::cout << "array size: ";
    std::cin >> n;
    std::cout << "enter " << n << " elements" << '\n';

    for (int i = 0; i < n; i++) {
        std::cin >> m[i];
    }

    std::cout << "where is it, from 0 to " << (n - 1) << ": ";
    std::cin >> position;
}

void Terminate::remove() {
    if(position > n-1) {
        std::cout << "Invalid position" << '\n';
    } else {
        element = m[position]; // for the UI
        // start from the position and go up
        for (int i = position; i < n; i++) {
            m[i] = m[i + 1];
        }
        n--; // it's shorter now
        std::cout << element << " terminated from " << position << '\n';
    }
}

void Terminate::display() {
    std::cout << "array after termination: " << '\n';
    for (int i = 0; i < n; i++) {
        std::cout << m[i] << '\n';
    }
}

int main(int argc, char const *argv[]) {
    Terminate T;
    T.getData();
    T.remove();
    T.display();

    return 0;
}

// area calculator, with function overloading
// same function with different amount of args
#include <iostream>
#include <cmath>

class FunctionOverload {
    double s;
public:
    // squares
    double area(double a) {
        return a*a;
    }
    // rectangles
    double area(double l, double b) {
        return l*b;
    }
    // triangle
    double area(double a, double b, double c) {
        s = (a + b + c)/2.0;
        return (sqrt(s * (s - a) * (s - b) * (s - c)));
    }
};

int main() {
    double x, y, z;
    int choice;
    FunctionOverload FO;

    // the menu, ooooh FANCY!
    std::cout << "=================================" << '\n';
    std::cout << "1: area of a square" << '\n';
    std::cout << "2: area of a rectangle" << '\n';
    std::cout << "3: area of a triangle" << '\n';
    std::cout << "choose 1 or 2 or 3: ";
    std::cin >> choice;

    if (choice == 1) {
        std::cout << "length of side of square: ";
        std::cin >> x;
        std::cout << "area of the square: " << FO.area(x) << '\n';
    } else if (choice == 2) {
        std::cout << "enter the lengths of the two sides of the rectangle:" << '\n';
        std::cin >> x >> y;
        std::cout << "area of the rectangle: " << FO.area(x, y) << '\n';
    } else if (choice == 3) {
        std::cout << "enter the length of the sides of the triangle:" << '\n';
        std::cin >> x >> y >> z;
        std::cout << "area of the triangle: " << FO.area(x, y, z) << '\n';
    } else {
        std::cout << "ERROR expected inputs[1, 2, 3]; got: " << choice << '\n';
    }

    return 0;
}

// array item insertion
#include<iostream>

// used just so we avoid global variables i guess
class Insertion {
private:
    int n, m[100], element, position;
public:
    void getData();
    void display();
    void insert();
};

void Insertion::getData() {
    std::cout << "array size: ";
    std::cin >> n;
    std::cout << "enter " << n << " elements in the array" << '\n';

    for (int i = 0; i < n; i++) {
        std::cin >> m[i];
    }

    std::cout << "what to insert in the array: ";
    std::cin >> element;
    std::cout << "where to insert it, from 0 to " << n << ": ";
    std::cin >> position;
}

void Insertion::insert() {
    if (position > n) {
        std::cout << "Invalid position" << '\n';
    } else {
        // from the end till you hit the position
        for (int i = n - 1; i >= position; i--) {
            // and shift everything one to the end
            m[i+1] = m[i];
        }
        n++; // so we dont cut off the last element
        // now insert
        m[position] = element;
        std::cout << element << " sucessfully inserted" << '\n';
    }
}

void Insertion::display() {
    std::cout << "done" << '\n';
    for (int i = 0; i < n; i++) {
        std::cout << m[i] << '\n';
    }
}

int main(int argc, char const *argv[]) {
    Insertion I;
    I.getData();
    I.insert();
    I.display();

    return 0;
}

// takes an array of ten elements and bubble sorts em
#include<iostream>

int main(int argc, char const *argv[]) {
    int array[10];
    int tmp;

    std::cout << "fill up the array: " << '\n';
    for (int i = 0; i < 10; i++) {
        std::cin >> array[i];
    }

    //bubble sort descending
    for (int i = 0; i < 10; i++) {
        for (int j = 0; j < 10; j++) {
            if (array[j] > array[j + 1]) {
                tmp = array[j];
                array[j] = array[j + 1];
                array[j + 1] = tmp;
            }
        }
    }

    // display
    std::cout << "sorted" << '\n';
    for (int i = 0; i < 10; i++) {
        std::cout << array[i] << " ";
    }
    std::cout << '\n';

    return 0;
}

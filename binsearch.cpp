// searches for stuff in an array (binary search)
#include<iostream>

class BinSearch {
private:
    int n, m[100], element, location;
public:
    void getData();
    void display();
    void search();
};

void BinSearch::getData() {
    std::cout << "array size: ";
    std::cin >> n;
    std::cout << "enter " << n << " elements for the array" << '\n';

    for (int i = 0; i < n; i++) {
        std::cin >> m[i];
    }

    std::cout << "what to search for: ";
    std::cin >> element;
}

void BinSearch::search() {
    int beginning, end, mid;
    location = -1;
    beginning = 0;
    end = n - 1;

    while (beginning <= end) {
        mid = (beginning + end)/2;
        // is our luck perfect?
        if (element == m[mid]) {
            location = mid;
            break; // forgive me
            // oh it's in the first half
        } else if (element < m[mid]) {
            end = mid - 1;
            // ah it's in the last half
        } else if (element > m[mid]){
            beginning = mid + 1;
        }
    }
}

void BinSearch::display() {
    if (location >= 0) {
        std::cout << "it's at position " << location << " from zero" << '\n';
    } else {
        std::cout << "it ain't here chief" << '\n';
    }
}

int main(int argc, char const *argv[]) {
    BinSearch S;
    S.getData();
    S.search();
    S.display();

    return 0;
}

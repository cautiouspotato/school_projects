// does some math stuff
#include<iostream>
#include<cmath>

class Quadratic {
private:
    double a, b, c, r1, r2, d;
public:
    void getData();
    void compute();
    void display();
};

void Quadratic::getData() {
    std::cout << "three coefficeints: ";
    std::cin >> a >> b >> c;
}

void Quadratic::compute() {
    d = (b*b) - (4*a*c);
    if (d == 0) {
        std::cout << "roots equal" << '\n';
        r1 = (-b-sqrt(d)) / (2*a);
        r2 = r1;
    } else if (d > 0) {
        std::cout << "roots are positive and different" << '\n';
        r1 = (-b+sqrt(d)) / (2*a);
        r2 = (-b-sqrt(d)) / (2*a);
    } else {
        std::cout << "roots are imaginary" << '\n';
    }
}

void Quadratic::display() {
    std::cout << "first root = " << r1 << '\n';
    std::cout << "second root = " << r2 << '\n';
}

int main(int argc, char const *argv[]) {
    Quadratic Q;
    Q.getData();
    Q.compute();
    Q.display();

    return 0;
}

// here is a fun thing to do
// give it a ginormous negative number and see what happens
// or give it an alphabet and see what happens
#include<iostream>

int main() {
	int number = -1;
	
	while(number <= -1) {
		std::cout << "Enter a number: ";
		std::cin >> number;
		
		if(number > 0) {
			std::cout << "ok" << std::endl;
		} else {
			std::cout << "enter a positive number" << std::endl;
		}
	}
		
	return 0;
}

